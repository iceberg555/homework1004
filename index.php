<!--Задача 1:-->
<!--Спросите у пользователя имя, фамилию, автобиографию (textarea).-->
<!--Проверьте, чтобы это были строки и в поле `автобиография`не было тегов.-->
<!--Выведите эти данные на экран в формате:-->
<!--Иванов Иван-->
<!--Родился …-->
<!--Вырос...-->
<!--Позаботьтесь о том, чтобы пользователь не мог вводить теги и таким образом сломать сайт.-->
<!--ЭТОГО НЕТ!!! Сделайте так, чтобы после отправки формы значения ее полей не пропадали.-->
<form action="" method="GET">
    <input type="text" name="name">
    <input type="text" name="lastName">
    <textarea name="autobiography"></textarea>
    <input type="submit" name="submit">
</form>
<?php
if (isset($_REQUEST['submit'])) {
    $lastName = strip_tags($_REQUEST['lastName']);
    $name = strip_tags($_REQUEST['name']);
    $autobiography = strip_tags($_REQUEST['autobiography']);
    echo "$lastName  $name <br> Родился и вырос: $autobiography";
}
?>


<!--Задача 7:-->
<!--Спросите возраст пользователя с помощью формы.-->
<!--Результат запишите в переменную $age.-->
<!--Сделайте так, чтобы после отправки формы значения ее полей не пропадали.-->
<form action="age" method="GET">
    <input name="age" value="<?php if (isset($_GET['age'])) echo $_GET['age']; ?>">
    <input type="submit">
</form>
<?php
if (isset($_REQUEST['submit'])) {
    $age = $_REQUEST['age'];
    echo $age;
}
?>

<!--Задача 8:-->
<!--Спросите у пользователя, какие из языков он знает: java, python, php, javascript etc.-->
<!--Выведите на экран те языки, которые знает пользователь.-->
<form action="" method="GET">
    <p>html<input type="checkbox" name="lang[]" value="html"></p>
    <p>css<input type="checkbox" name="lang[]" value="css"></p>
    <p>php<input type="checkbox" name="lang[]" value="php"></p>
    <p>javascript<input type="checkbox" name="lang[]" value="javascript"></p>
    <input type="submit">
</form>
<?php
if(isset($_REQUEST['lang']))
{
    echo 'Вы знаете: ' . implode(',', $_REQUEST['lang']);
}
?>


<!--//Задача 14:-->
<!--//Отправьте с помощью GET-запроса два числа. Выведите его на экран сумму этих чисел.-->
<?php
echo $_GET ['get1'] + $_GET ['get2'];
?>
<!--//Пусть с помощью GET-запроса отправляется число. Оно может быть или 1, или 2. Сделайте так, чтобы если-->
<!--//передано 1 - на экран вывелось слово 'привет', а если 2 - то слово 'пока'.-->
 <?php
 if($_GET['get'] == 1)
     echo 'Привет';
 if($_GET['get'] == 2)
     echo 'Пока'; ?>


<!--//Задача 15:-->
<!--//Дан массив.-->
<!--//Сделайте так, чтобы с помощью GET-запроса можно было вывести любой элемент этого массива.-->
<!--//Для этого вручную сделайте ссылку для каждого элемента массива.-->
<!--//Переходя любой ссылке мы должны увидеть на экране соответствующий элемент массива.-->
<!--//Ссылки должны выводились с помощью цикла foreach 		автоматически в соответствии с количеством элементов в массиве.-->
<?php
$arr =['a' , 'b', 'c', 'd'];
if (isset($_GET['get']) and isset($arr[$_GET['get']])) {
    echo $arr[$_GET['get']];
}
echo '<br>';
foreach ($arr as $key =>$elem) {
    echo "<a href=\"?get=$key\">link $elem</a> ";
}
?>